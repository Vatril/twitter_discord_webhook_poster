import json
import twitterconnector
import discord_poster
import time


def check_tweets(first=False):
    try:
        with open("users.json", 'r+') as user_json:
            userlist = json.load(user_json)
            for user in userlist['users']:
                try:
                    tweets = twitterconnector.get_tweets(user['name'])
                    for tweet in reversed(tweets):
                        if tweet['id'] > user['last']:
                            user['last'] = tweet['id']
                            if not first:
                                discord_poster.post_tweet(tweet)
                                time.sleep(2)
                except:
                    print("error handling will be maybe implemented later")
            user_json.seek(0)
            user_json.write(json.dumps(userlist, indent=4))
    except:
        print("error handling will be maybe implemented later")


check_tweets(True)
while True:
    check_tweets(False)
    time.sleep(60)
