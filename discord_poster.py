import json
import requests


def post_tweet(tweet):
    with open("secrets.json") as urls:
        url = json.load(urls)['poster-webhook-url']
        color = int(tweet['user']['profile_link_color'], 16)
        embeds = [{
            'color': color,
            'fields': [
                {
                    'name': 'tweeted:',
                    'value': tweet['text']
                }
            ]
        }]
        if 'extended_entities' in tweet:
            if 'media' in tweet['extended_entities']:
                for media in tweet['extended_entities']['media']:
                    if media['type'] == 'photo':
                        embeds.append({'image': {'url': media['media_url_https']}, 'color': color})

        embeds.append({
            'color': color,
            'fields': [
                {
                    'name': 'Link:',
                    'value': "https://twitter.com/{0}/status/{1}"
                    .format(tweet['user']['screen_name'], tweet['id'])
                }
            ]
        })
        payload = {
            'username': tweet['user']['name'],
            'avatar_url': tweet['user']['profile_image_url'],
            'embeds': embeds

        }
        headers = {
            'content-type': "application/json"
        }


        res = requests.request("POST", url, data=json.dumps(payload), headers=headers)
