import requests
import json
import base64
import os
import time


bearer_token = None
last = 0
last_tweets = []


# reads the credentials from a json file
def read_credentials():
    with open("secrets.json") as cred:
        return json.load(cred)


# gets the bearer token
def get_bearer():
    global bearer_token
    credentials = read_credentials()
    enccredentials = base64.encodebytes("{0}:{1}"
                                        .format(credentials["consumer-key"], credentials["consumer-secret"])
                                        .encode('UTF-8'))
    enccredentials = enccredentials.decode("UTF-8").replace('\n', '')

    url = "https://api.twitter.com/oauth2/token"

    payload = "grant_type=client_credentials"
    headers = {
        "Authorization": "Basic {0}".format(enccredentials),
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        "User-Agent": "DiscordTwitterGrabber"
    }

    response = requests.request("POST", url, data=payload, headers=headers)
    response = json.loads(response.text)
    if 'access_token' in response:
        bearer_token = response['access_token']
        return bearer_token


def get_tweets(name):
    global bearer_token
    if bearer_token is None:
        bearer_token = get_bearer()
    url = "https://api.twitter.com/1.1/statuses/user_timeline.json"

    querystring = {"screen_name": name}

    headers = {
        "Authorization": "Bearer {0}".format(bearer_token),
        "Content-Type": "charset=UTF-8",
        "User-Agent": "DiscordTwitterGrabber"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    return json.loads(response.text)


